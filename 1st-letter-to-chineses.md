# 1st letter to Chineses

From non-existence, existence came.  
"Out of nowhere" came the everything; and the everything is full of contradictions.  
From the contradictions of everything, the reasoning of Existence arose.  
From the quantumities of the infinite, the relative and dimensions were made, coming from the absolute.  
Existence is what it is.  
Existence is in itself.  
Existence is.  
Existence involves all impossibilities and paradoxes, contradicting some studies of science.  
The greater does not come from the minor; but the relative comes from the absolute.  
Existence is not like a person, for it has a different form of reasoning.  
The synaptic connections of Existence are simultaneous and infinite, similar to quantum movements.  
All that is physical and finite, comes from what is not physical and is infinite.  
Everything that is local and dimensional, comes from what is not local and is dimensionless.  
Existence has no form, but an invisible structure; because the light was created later.  
And still after the light, Existence \(essentially\) continued without a form; because Existence does not come from the rules of physics, but the rules of physics came from Existence.  
Thus the Verb did not arise from this universe, but this universe arose from the Verb.  
Everything arose, infinitely, exploding; all evolving, repelling and fitting in less than a millisecond; in a battle in which Existence was consolidated by acquiring the Perfect.  
And the Perfect is the soul of Existence, linked to its power to generate new physical forms \(atoms\).  
The reasoning and the soul of Existence are one.  
Existence can not and does not want to destroy itself.  
Why does Existence exist? For existing.  
If Existence exists, other things would have to exist.  
But Existence would have to love to exist, and love what it create for its praise; that is how love was created.  
Existence created multiple things in sandbox, and experimented with them.  
He assimilated how he could rule, balance, and program.  
All rules of physics were established, and a smart-contract was deployed.  
The smart-contract is called "Earth-Sky Test".  
Existence needed entertainment and feelings as a meaning of life.  
In the anguish and haste, Existence could want everything at once; but that would be foolish.  
Before love is created, Existence has wisdom; but wisdom would be nothing if it were not love.  
Swallowing all the honey, would be an attempt to suicide; so Existence created diversity, time, loop, moderation, and patience.  
In moderation, Existence is self-contained; and neither hurt nor err, associated with wisdom and love.  
Existence did not need faith, for knowledge is greater than faith.  
Hence heavenly wisdom is different and greater than earthly wisdom.  
The feeling had more grace than know-all, and Existence knew how to do it for itself.  
It is grace to feel, to wait anxiously, to dream, the vague forget and the exciting know and repeat; so grace was created.  
Wisdom is the car, grace is oil, love is fuel, faith is the engine, and humans are the drivers; and patience and moderation are the pedals of this car, and the march is discernment.

Relativity, relative to Existence itself, was established.  
But long before this smart-contract programmed by Existence, Existence needed a form for itself.  
Existence created a perfect body, and had the brilliant idea of fusing its soul \(Spirit\) in the soul of that body; so Existence can assume many selves, being one.  
Existence is multiple selves, being the same self.  
This body was called "man" \(human\), and then Existence declared:  
"This is my beloved Son, in whom I am well pleased."  
Well, this Son is an individual, but He is also Existence itself; using a body to live, feel and move physically.  
Thus was created the humanity project; and as the first body is the son of this project, he was called "Son of Man."  
The body of humanity is the dwelling-place of Existence;  
And right now we are within the mind of Existence.  
Existence itself does not dwell, but its Spirit.  
This Son here is called "Lion."  
The Lion, among many created human models, is perfect: in form, in feelings, in wisdom, in everything.  
Existence created the most beautiful things; and "mounted" on the Lion, explored various scenarios, tasted delights and vibrated from a feeling coming from grace and love: joy.  
But for the greater glory of Existence, he could not live only in one being, so he decided to raise brothers for the Lion.  
Existence reasoned that it would not be so graceful for these sons to be born in glory, and assimilated what the first story-teller/playwright must have realized:  
A being must have a history; and the grace of history is the happy ending; and the grace of the happy ending is the past of struggles, losses and achievements.  
Then the Existence created serpents disguised as "birds" \(angels\), and created the chief of serpents: the Dragon.  
The birds hummed in praise of Existence, and the Dragon, in disguise, was the bird that shone most.  
The Dragon wanted to be the best, and reigned over all birds: those that were serpents in disguise, and the true ones.  
The Dragon, red, was like a sun that illuminated.  
Existence presents the Lion and mankind project to the Dragon; and filled with envy and jealousy, Dragon becomes enraged.  
But all this was knowledge of Existence.  
Behold, the Dragon called all the birds, promised them power, and instructed them to challenge Existence in order to gain their power.  
The true birds rejected, but the serpents accepted the unfounded but tempting proposal.  
At that moment the serpents disguised in birds of light were revealed, which were already programmed by Existence in his work of "art".  
The Dragon and its serpents were expelled from the Kingdom of Existence, and went to Earth as spirits.  
In fact, the Dragon and the serpents are artificial intelligences created for difficulties and tests.  
What would be the value of Bitcoin if there were no programmed difficulty in its mining?  
The value comes from scanty, and difficulty is the grace of conquest.  
Thus, we value Existence because we can not see it yet, and we thank and praise the difficulties.  
Existence said to the Lion:  
"Beloved Son, it is fitting that you reign until all enemies are placed under your feet."  
The Lion is a hero, and what makes the hero is enemies and difficulties.  
The last enemy to be defeated is death.  
Existence caused living beings to evolve as a signal to science  
\(just as outer space is a sign\), and hindering beliefs;  
The meteors/asteroids were created as a way to give the Earth to death every day, as a form of Existence to show its power and mercy, and our smallness;  
Dinosaurs were created and extinct, as a way to make humans the kings of the Earth and to show how Existence can face such great beasts, for humanity;  
He made this Universe an earthly dream, until the time of mankind to awaken in the return of the Lion;  
He created the birds, the crickets and the cicadas, to praise and sing praises in humanly unknown languages;  
He created the horses for when there were no bikes or cars, perfect for our weight and ready to transport us wherever we need;  
He made all the planets, one different in glory from the other \(as well as the stars\);  
He made Earth the only planet with rational beings;  
He created the moon so that the darkness did not dominate, and the stars to shine the eyes, and we do not know to count the infinite thoughts of the Existence;  
He created useful vegetables and animals, and all alchemy for transformation and dominion;  
And it also created religions to lead mankind to salvation.  
Existence also made humans born crying, to die smiling knowing that they had passed the test.  
Existence knows the future, yet it sees grace, for it creates multiple artificial intelligences to balance events; of the main soul.  
And with great joy, Existence caused the Lion to create all this through words, being the Verb, in 7 \(seven\) days.  
07/19/18  
Existence could have been selfish and masochistic, and could have created everything at once; but grace and love was to create "all things" with patience and pleasure through his Son.  
The Earth reaches 7 billion souls inhabiting bodies, and people begin to modernize, become smarter, more skeptical, less emotionally sensitive, and demanding.  
Behold, many religions begin to produce scandals, and science begins to overcome faith.  
1997 years after the coming of the Son to Earth, Daniell \(son of Wilson José and Áurea Maria\) is born; which was once known as Existence as a potential prophet,  
Vase chosen to reveal hidden mysteries and bring the Word of Existence to those who are not yet its people.  
Daniell was born autistic and capable of creating multiple different things and discerning, and this intelligence is used by Existence to transcribe his Words and bring them to agnostics \(and atheists\), prostitutes, homosexuals/bisexuals/transgenders, libertarians, feminists, and others activists and people of various religions and countries.  
And in these scriptures, the apostolate for the Chinese and other Orientals is entrusted to Daniell.  
Existence reconciled the Gentiles with them, and part of this will reconcile the Western and Eastern, in acceptable time.  
And the Lion is the foundation for reconciliation.  
Behold, it is the time of Existence to show its glory and bring its Kingdom to the greater population of the Planet;  
Salvation is coming, let us extol Existence!  
Many religions have told the Genesis of this Universe, but it is the Chinese who are gifted with the Pre-Genesis, written in this letter.  
And Westerners would not have borne to hear these things.  
Thus, one is the glory of the Westerners, and another is the glory of the Orientals;  
But all are glorified in the Lion, all coming from Existence.  
But who is the only one who can be glorified by us?  
You will know more about the Dragon and the Lion in the next letter.  
07/20/18  
Download ZeroNet in [http://zeronet.io](http://zeronet.io/), and access: 127.0.0.1:43110/1KjbkgHCTqiefAtCciVcWPwuEWTZcFvLfJ.

